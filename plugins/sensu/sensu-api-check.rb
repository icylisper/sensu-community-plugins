#!/usr/bin/env ruby
#
#
# This plugin checks if Sensu-api is running
#

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli'
require 'json'
require 'rest_client'

class CheckRabbitMQ < Sensu::Plugin::Check::CLI

  option :host,
    :description => "API Host",
    :short => '-w',
    :long => '--host HOST',
    :default => 'localhost'


  option :port,
    :description => "Sensu API port",
    :short => '-P',
    :long => '--port PORT',
    :default => '4567'


  def run
    host     = config[:host]
    port     = config[:port]
    RestClient.get("http://#{host}:#{port}/health"){ |response, request, result, &block|
      case response.code
      when 200
        ok "Sensu-api working"
      when 503
        critical "503: Sensu API Not working"
      else
        critical "Sensu API Not working"
      end
    }
  end
end


#!/bin/bash

NEXUSHOST=localhost:8081
NEXUSSTATUSURL="http://${NEXUSHOST}/service/local/status"

STATE=`curl -s $NEXUSSTATUSURL | grep state | sed 's/\s*[</]*state>//g'`

if [ -z "$STATE" ]; then
  echo "Unable to retrieve Nexus state"
  exit 2
else
  if [ "STARTED" == $STATE ]; then
    echo "Nexus OK"
    exit 0
  else
    echo "Nexus in unexpected state: $STATE"
    exit 2
  fi
fi





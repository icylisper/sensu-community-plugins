#!/usr/bin/env ruby
#
# ZooKeeper wchs Metrics
# ===
#
# Copyright 2011 Sonian, Inc.
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require "rubygems"
require "sensu-plugin/metric/cli"
require "socket"

class ZooKeeperWchsMetrics < Sensu::Plugin::Metric::CLI::Graphite

  option :host,
         :description => "ZooKeeper host",
         :short => '-h',
         :long => '--host HOST',
         :default => 'localhost'

  option :port,
         :description => "ZooKeeper client port",
         :short => '-P',
         :long => '--port PORT',
         :default => 2181,
         :proc => proc { |a| a.to_i }

  option :scheme,
         :description => "Metric naming scheme, text to prepend to metricname.value",
         :long => "--scheme SCHEME",
         :default => "sensu.#{ENV["CLUSTER_NAME"]}.#{ENV["FACET_NAME"]}#{ENV["FACET_INDEX"]}.zookeeper"


  def wchs(host, port)
    res = nil
    TCPSocket.open(host, port) do |s|
      s.puts "wchs"
      res = s.read
    end

    begin
      connections, _, _, paths, _, _, watches_str = res.split
      _, watches = watches_str.split(":")

      { :watches => watches, :connections => connections, :paths => paths }
    rescue
      puts "Destructuring failed. Msg #{res}"
      {}
    end
  end

  def run
    timestamp = Time.now.to_i
    host = config[:host]
    port = config[:port]

    res = wchs(host, port)
    res.each_pair do |metric, value|
      output([config[:scheme], metric.to_s].join('.'), value, timestamp)
    end
    ok
  end

end

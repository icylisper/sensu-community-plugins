#!/opt/sensu/embedded/bin/ruby

require 'rest_client'
require 'json'
require 'tabulate'

host = 'localhost'

res =  RestClient.get "http://#{host}:4567/checks"
r = JSON.parse(res)

def get_args(cmd)
  cmd.split('/')[-1]
end

source = r.map {|v| [v['name'],
                     v['interval'],
                     v['refresh'],
                     v['subscribers'],
                     v['handlers'],
                     get_args(v['command'])
                    ]}
labels = ['Check','Interval','Refresh','Subscribers', 'Handlers', 'Args']
puts tabulate(labels, source, :indent => 4, :style => 'plain_alt')

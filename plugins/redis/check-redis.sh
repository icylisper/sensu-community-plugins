#!/bin/bash


/opt/redis/bin/redis-cli ping

if [[ "$?" == "0" ]]; then
    echo "Redis is running fine"
    exit 0
else
    echo "Redis is not running"
    exit 2
fi


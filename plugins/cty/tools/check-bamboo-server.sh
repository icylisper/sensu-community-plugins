#!/bin/bash

HOST=localhost
PORT=8085

# for now just do netcat, REST call once we have an api user
nc -z $HOST $PORT 2>&1 >/dev/null
RES=$?

if [ 0 -eq $RES ]; then
  echo "Bamboo Server listening on port $PORT"
  exit 0
else
  echo "Cannot connect to Bamboo Server port $PORT"
  exit 2
fi


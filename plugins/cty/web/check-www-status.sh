#!/bin/bash

HOST=localhost:9000
STATUSURL="http://${HOST}/status"
VERSIONURL="http://${HOST}/version"

STATE=`curl -s $STATUSURL`

if [ -z "$STATE" ]; then
  echo "Unable to retrieve website status page"
  exit 2
else
  VERSION=`curl -s $VERSIONURL`
  if [ "ok" == $STATE ]; then
    echo "Web instance OK, version $VERSION"
    exit 0
  else
    echo "status page reports unexpected state: $STATE, version $VERSION"
    exit 2
  fi
fi


#!/usr/bin/env ruby
#
# Plugin to check HDFS Corruption
# ===
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli'
require 'json'
require 'net/http'
require 'crack'

class CheckYarn < Sensu::Plugin::Check::CLI

  option :host,
    :description => "HDFS JMX host",
    :short => '-w',
    :long => '--host HOST',
    :default => 'localhost'

  option :port,
    :description => "HDFS JMX port",
    :short => '-P',
    :long => '--port PORT',
    :default => '50070'

  def run
    res = hdfs_corrupt_blocks

    if res == 0
      ok 'HDFS Healthy'
    else
      critical 'HDFS Corrupt'
    end
  end

  def hdfs_corrupt_blocks
    host     = config[:host]
    port     = config[:port]

    begin
      url = "http://#{host}:#{port}/jmx?qry=Hadoop:service=NameNode,name=FSNamesystem"
      res = Net::HTTP.get_response(URI.parse(url)).body.gsub("\n", "")
      stats = JSON.parse(res)
      stats['beans'][0]['CorruptBlocks']    
    end
  end

end


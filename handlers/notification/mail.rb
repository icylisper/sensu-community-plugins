#!/usr/bin/env ruby
#
# Sensu Mail Handler
# ===
# This handler uses 'pony' to send out mail.
# Configuration for mail in mail.json 

# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-handler'
require 'timeout'
require 'pony'

class IRC < Sensu::Handler

  def set_mail_opts

    opt = settings["mail"]["smtp"]
    if opt.is_a?(Hash)
      opts = {}
      opt.map { |k,v| opts[k.to_sym] = v }
      Pony.options = { :via => :smtp, :via_options => opts }
    else
      Pony.options = { :via => :smtp }
    end
  end

  def subject
    "Sensu: #{@event['client']['name']} [ #{@event['check']['name']} ]"
  end

  def body
    @event['notification'] || [@event['client']['name'], @event['check']['name'], @event['check']['output']].join(' : ')
  end

  def to
    settings["mail"]["to"].join(", ")
  end

  def handle
    begin
      timeout(10) do

        set_mail_opts
        Pony.mail({:to => to , :subject => subject, :body => body })
        
        puts "mail -- sent mail: #{subject}"
      end
    rescue Timeout::Error
      puts "mail -- timed out while attempting to send mail: #{subject}"
    end
  end

end

#!/usr/bin/env ruby
#
# Plugin to check HDFS Capacity (Size)
# ===
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli'
require 'json'
require 'net/http'
require 'crack'

class CheckYarn < Sensu::Plugin::Check::CLI

  option :host,
    :description => "HDFS JMX host",
    :short => '-w',
    :long => '--host HOST',
    :default => 'localhost'

  option :port,
    :description => "HDFS JMX port",
    :short => '-P',
    :long => '--port PORT',
    :default => '50070'

  option :size,
    :description => "Threshold size in GB",
    :short => '-s',
    :long => '--size SIZE',
    :default => 20

  
  def run
    res = hdfs_size

    if res.to_i > config[:size].to_i
      ok "HDFS: size #{res}"
    else
      critical "HDFS Full: size #{res}"
    end
  end

  def hdfs_size
    host     = config[:host]
    port     = config[:port]

    begin
      url = "http://#{host}:#{port}/jmx?qry=Hadoop:service=NameNode,name=FSNamesystem"
      res = Net::HTTP.get_response(URI.parse(url)).body.gsub("\n", "")
      stats = JSON.parse(res)
      stats['beans'][0]['CapacityRemainingGB']    
    end
  end

end

